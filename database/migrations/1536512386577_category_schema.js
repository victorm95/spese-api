'use strict'

const Schema = use('Schema')

class CategorySchema extends Schema {
  up() {
    this.create('categories', table => {
      table.increments()
      table.string('name', 50).notNullable()
      table.string('icon', 20).notNullable()
      table.string('color', 20).notNullable()
      table.timestamps(true, true)
    })
  }

  down() {
    this.drop('categories')
  }
}

module.exports = CategorySchema
