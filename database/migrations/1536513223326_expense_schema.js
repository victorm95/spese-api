'use strict'

const Schema = use('Schema')

class ExpenseSchema extends Schema {
  up() {
    this.create('expenses', table => {
      table.increments()
      table.date('date').notNullable()
      table.string('title', 50).notNullable()
      table.text('description').nullable()
      table.decimal('value', 2).notNullable()
      table
        .integer('category_id')
        .unsigned()
        .nullable()
      table.foreign('category_id').references('categories.id')
      table.timestamps(true, true)
    })
  }

  down() {
    this.drop('expenses')
  }
}

module.exports = ExpenseSchema
