'use strict'

const Model = use('Model')

class Expense extends Model {
  category() {
    return this.belongsTo('App/Models/Category')
  }
}

module.exports = Expense
