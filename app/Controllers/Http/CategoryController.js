'use strict'

const Category = use('App/Models/Category')

class CategoryController {
  async index({ request }) {
    const { page = 1 } = request.get()
    return await Category.query().paginate(page)
  }

  async store({ request, response }) {
    const category = await Category.create(request.post())
    response.status(201).send(category)
  }

  async show({ response, params }) {
    try {
      return await Category.findOrFail(params.id)
    } catch (e) {
      response.status(404).send({ message: 'Category not found' })
    }
  }

  async update({ request, response, params }) {
    try {
      const category = await Category.findOrFail(params.id)
      category.merge(request.post())
      if (await category.save()) {
        return category
      } else {
        response.status(500).send({ message: 'Error saving the category' })
      }
    } catch (e) {
      response.status(404).send({ message: 'Category not found' })
    }
  }

  async destroy({ response, params }) {
    try {
      const category = await Category.findOrFail(params.id)
      await category.delete()
      response.status(204).send()
    } catch (e) {
      response.status(404).send({ message: 'Category not found' })
    }
  }
}

module.exports = CategoryController
