'use strict'

const Expense = use('App/Models/Expense')

class ExpenseController {
  async index({ request }) {
    const { page = 1 } = request.get()
    return await Expense.query()
      .with('category')
      .paginate(page)
  }

  async store({ request, response }) {
    const expense = await Expense.create(request.post())
    response.status(201).send(expense)
  }

  async show({ response, params }) {
    try {
      const expense = Expense.findOrFail(params.id)
      return expense
    } catch (e) {
      response.status(404).send({ message: 'Expense not found' })
    }
  }

  async update({ request, response, params }) {
    try {
      const expense = Expense.findOrFail(params.id)
      expense.merge(request.post())
      if (await expense.save()) {
        return expense
      } else {
        response.status(500, {
          message: 'Error saving the expense'
        })
      }
    } catch (e) {
      response.status(404, { message: 'Expense not found' })
    }
  }

  async destroy({ response, params }) {
    try {
      const expense = Expense.findOrFail(params.id)
      expense.delete()
      response.status(204).send()
    } catch (e) {
      response.statu(404).send({ message: 'Expense not found' })
    }
  }
}

module.exports = ExpenseController
